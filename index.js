var AWS = require('aws-sdk');
var DOC = require('dynamodb-doc');
var dynamo = new DOC.DynamoDB();

var google = require('googleapis');

var networks = ["YouTube"];

//Callback functions
var error = function (err, response, body) {
    console.log('ERROR [%s]', err);
};
var success = function (data) {
    console.log('Data [%s]', data);
};

var cb = function(err, data) {
    if(err) {
        console.log(err);
    } else {

    }
};

var messageOutput = "Results\n";

exports.handler = function(event, context) {
    var cb = function(err, data) {
        if(err) {
            console.log('error on RetrieveSocialFeed: ',err);
            context.done('Unable to retrieve Social Site Data', null);
        } else {
            data.Items.forEach(function(item) {
                if (item && item.SiteId) {
                    networks.forEach(function (elem) {
                        if (item[[elem] + "Status"]) {
                            loopThroughNetworks(elem, item[[elem] + "Id"], item.SiteId);
                        }
                    });
                }
            });
        }
    };

    var params = {
        TableName : "SocialSiteSettings",
        FilterExpression: "SiteStatus = :siteStatus",
        ExpressionAttributeValues: {
            ":siteStatus": true,
        }
    };

    dynamo.scan(params, cb);
};

function loopThroughNetworks(network, networkId, siteId) {
    // call the correct function for the network in question returning a count of items found
    eval([network] + "Feed")(networkId, siteId);
}

function YouTubeFeed(networkId, siteId) {

    var youtube = google.youtube('v3');

    var params = { part: 'snippet,contentDetails,status,id',  playlistId: networkId, auth: 'xxxx', maxResults: 50};

    youtube.playlistItems.list(params, function (err, response) {
        if (err) {
            console.log('Encountered error', err);
        } else {

            var videos = response.items;

            console.log("\nYouTube: " + networkId + " - " + videos.length);

            for (i in videos) {

                //console.log(videos[i].snippet);

                if (typeof videos[i].snippet.resourceId.videoId !== 'undefined') {

                    var DateCreated = Math.round((+new Date(Date.parse(videos[i].snippet.publishedAt.replace(/( \+)/, ' UTC$1')))/1000));

                    var item = {
                        ItemId: siteId + videos[i].id,
                        SiteId: Number(siteId),
                        FeedType: "YouTube",
                        DateCreated: DateCreated,
                        Title: videos[i].snippet.title,
                        Active: true,
                        VideoId: videos[i].contentDetails.videoId
                    };

                    var response = dynamo.putItem({TableName:"SocialFeedsData", Item:item}, function(err, data) {
                        if (err) {
                            console.log("Error in putItem "+err);
                        } else {
                            console.log("Successfully Inserted");
                        }
                    });
                }

            }

        }
    });

}
